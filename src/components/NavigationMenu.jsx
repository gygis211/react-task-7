import React from "react";
import { NavLink } from "react-router-dom";
import "../styles/navigationMenu.css";


function NavigationMenu () {
    return (
      <nav className="header">
          <NavLink className="headerText header1" to="/"><h1>Home</h1></NavLink>
          <NavLink className="headerText header2" to="/admin"><h1>Admin</h1></NavLink>
          <NavLink className="headerText header3" to="/news"><h1>News</h1></NavLink>
          <NavLink className="headerText header4" to="/profile"><h1>Profile</h1></NavLink>
      </nav>
    );
}

export default NavigationMenu;
