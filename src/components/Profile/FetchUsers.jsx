import React from "react";
import { useDispatch, useSelector } from "react-redux";

import { fetchUsers } from "../../reducers/userReduser";

const FetchUsers = () => {
    const users = useSelector(state => state.userReduser.users);
    const dispatch = useDispatch();

    return (
        <div className="App">
            <button className='button' onClick={() => dispatch(fetchUsers())}>ПОЛУЧИТЬ ЮЗЕРОВ</button>
            <div className='users'>
                {users.map(user =>
                    <div className="user">
                        {user.name}
                    </div>
                )}
            </div>
        </div>
    );
}

export default FetchUsers;