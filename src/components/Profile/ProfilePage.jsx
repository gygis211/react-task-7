import React, { useState } from "react";
import { connect } from "react-redux";
import Input from './Input.jsx'
import FetchUsers from './FetchUsers'

function ProfilePage(props) {
    const [editedName, setName] = useState('');
    const [editedSurname, setSurname] = useState('');
    const [editedCard, setCard] = useState('');

    const handleEdit = () => {
        let user = {
            name: editedName ? editedName : props.profile.name,
            surname: editedSurname ? editedSurname : props.profile.surname,
            card: editedCard ? editedCard : props.profile.card,
        }

        props.editUser(user)

        setName('')
        setSurname('')
        setCard('')

    }

    return (

        <div>
            <h1>Данные пользователя</h1>
            <div className="post">
                <div className="inputForm">
                    <strong>Имя: <br />{props.profile.name}</strong><br />
                    <strong>Фамилия: <br />{props.profile.surname}</strong><br />
                    <strong>Номер карты: <br />{props.profile.card}</strong>
                </div>
                Имя <Input className="inputForm" value={editedName} setValue={setName} />
                фамилия <Input className="inputForm" value={editedSurname} setValue={setSurname} />
                Номер карты <Input className="inputForm" value={editedCard} setValue={setCard} flag={true}/>
                <button onClick={handleEdit}>Изменить</button>
                <FetchUsers />
            </div>
        </div>
    )
}

const mapStateToProps = (state) => ({
    profile: state.profile
});

const mapDispatchToProps = (dispatch) => ({
    editUser: (data) => { dispatch({ type: 'EDIT_USER', data }) },
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ProfilePage);
