import React from 'react';

const HandleError = (props) => {
    const textMessage = () => {
        if (props.length > 1 && props.length < 16) {
            return 'Указано 15 символов. Нужно указать 16';
        } else if (props.length === 16) {
            return 'Указано 16 символов'
        } else if (props.length > 16)
            return 'Указано больше символов чем нужно. Нужно указать 16'
    }

    return (
        <div>
            <p>{textMessage()}</p>
        </div>
    )
}

export default React.memo(HandleError, (prevProps, nextProps) => {
    if (nextProps.length > 14 && nextProps.length < 17) {
        return false;
    } else if (prevProps.length > 14 && prevProps.length < 17) {
        return false;
    }
    return true;
})