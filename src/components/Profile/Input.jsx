import React from 'react';
import HandleError from './HandleError'

const Input = (props) => {

    return (
        <div>
            <input value={props.value} type="text" onChange={(e) => props.setValue(e.target.value)} />
            {props.flag && <HandleError length={props.value.length}/>}
        </div>
    )
}

export default React.memo(Input);